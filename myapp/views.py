# Create your views here.
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponseRedirect

from fileupload.myapp.models import Document
from fileupload.myapp.forms import DocumentForm
from django.core.urlresolvers import reverse

def list(request):
	if request.method=='POST':
		form=DocumentForm(request.POST,request.FILES)
		if form.is_valid():
			newdoc=Document(docfile=request.FILES['docfile'])
			newdoc.save()

			return HttpResponseRedirect(reverse('myapp.views.list'))
	else:
		form=DocumentForm()


	documents=Document.objects.all()

	return render_to_response(
		'myapp/list.html',
		{'documents':documents,'form':form},
		context_instance=RequestContext(request)
		)

def listview(request):
	if request.method=='POST':
		form=DocumentForm(request.POST,request.FILES)
		if form.is_valid():
			newdoc=Document(docfile=request.FILES['docfile'])
			newdoc.save()

			return HttpResponseRedirect(reverse('myapp.views.listview'))
	else:
		form=DocumentForm()


	documents=Document.objects.all()

	return render_to_response(
		'myapp/list.html',
		{'documents':documents,'form':form},
		context_instance=RequestContext(request)
		)
