from django.db import models
import datetime
import os
import uuid
# Create your models here.
def get_file_path(instance,filename):
	ext=filename.split('.')[-1]
	filename="%s.%s" % (uuid.uuid4(),ext)
	return os.path.join(instance.directory_string_var,filename)

class Document(models.Model):
	docfile=models.FileField(upload_to=get_file_path)

	now=datetime.datetime.now()
	directory_string_var = 'image/%s/%s/%s/'%(now.year,now.month,now.day)
